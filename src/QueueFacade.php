<?php

namespace Lar\Queue;

use Illuminate\Support\Facades\Facade as FacadeIlluminate;

/**
 * Class Facade
 * @package Lar\WS
 */
class QueueFacade extends FacadeIlluminate
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return QueueInstance::class;
    }
}
